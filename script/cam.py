#!/usr/bin/python3

from os import wait
import face_recognition
import cv2
from face_recognition.api import face_locations
import rospy
import gc
from std_msgs.msg import Float32MultiArray
from std_srvs.srv import Empty,EmptyResponse
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
from skuba_athome_msgs.srv import DynamixelControllers
import time 

class Face_Recognition(object):

    def __init__ (self):    

        #Add_face_to_recognitions
        # rospy.set_param("Show_Face",True)
        self.person1_image = face_recognition.load_image_file("/home/skuba/skuba_ws/src/camera_tracking/image/img1.png") #------- change path to person picture
        self.person1_face_encoding = face_recognition.face_encodings(self.person1_image)[0]

        self.person2_image = face_recognition.load_image_file("/home/skuba/skuba_ws/src/camera_tracking/image/img2.png") #------- change path to person picture
        self.person2_face_encoding = face_recognition.face_encodings(self.person2_image)[0]

        self.person3_image = face_recognition.load_image_file("/home/skuba/skuba_ws/src/camera_tracking/image/img3.png") #------- change path to person picture
        self.person3_face_encoding = face_recognition.face_encodings(self.person3_image)[0]

        self.known_face_encodings = [self.person1_face_encoding,self.person2_face_encoding,self.person3_face_encoding]
        self.known_face_names = ["Mint","PP","Proud"] #------- change name

        self.face_locations = []
        self.face_encodings = []
        self.face_names = []
        self.process_this_frame = 1
        self.frame = []
        self.name = -1
        self.node_call = True
        self.bridge = CvBridge()
        self.middle = [0,0,0,0]
        rospy.init_node("face_recog", anonymous=True)
        self.cv_image = None
        self.facetrack = False
        self.pub = rospy.Publisher("face_recognition", Float32MultiArray, queue_size=10)
        rospy.Subscriber("/camera/color/image_raw", Image, self.callback )
        self.add_angle_pan = rospy.ServiceProxy('/realsense_pan_controller/command', DynamixelControllers)
        self.add_angle_tilt = rospy.ServiceProxy('/realsense_tilt_controller/command', DynamixelControllers)
        self.add_angle_pan(0)
        self.add_angle_tilt(0)
        self.pan_degree = 0
        self.tilt_degree = 0
        self.avg_r_x = []
        self.avg_r_y = []
        self.n_avg = 5
        self.val_rx = 0
        self.val_ry = 0
        enable = rospy.Service('/facetrack/enable', Empty, self.enable)
        s = rospy.Service('/facetrack/disable', Empty, self.disable)
        print("Init success")

    def enable(self,data):
        self.facetrack = True
        print("Enable")
        return EmptyResponse()
    
    def disable(self,data):
        self.facetrack = False
        print("Disable")
        return EmptyResponse()
        
    def callback(self,data):
        
        if self.facetrack :
            try:
                self.cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
                pub_data = Float32MultiArray()
                #pub_data.data = self.recognition()
                #print(pub_data.data)
                self.pub.publish(pub_data)
                
            except CvBridgeError as e:
                print(e)

    def recognition(self):
        # while not rospy.is_shutdown():
        

        # if self.cv_image is None:
        #     continue

        cv_image = self.cv_image
        if cv_image is None:
            print("Cannot find image.")
            return 1
        #cv2.imshow("pic1",cv_image)

        #print("reviced image")
        known_face_location = []

        # gray = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
        # cv2.imshow('gray', gray)

        small_frame = cv2.resize(cv_image, (0, 0), fx=0.5, fy=0.5)

        rgb_small_frame = small_frame[:, :, ::-1]
        #print(len(rgb_small_frame),len(rgb_small_frame[0]))
        #print("process : Face Location")
        face_locations = face_recognition.face_locations(rgb_small_frame, model="cnn")
        #print(face_locations)

        #print("process : Face Encoding")
        face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)
        # print(face_encodings)

        for i in range(len(face_locations)):
            # print(i)
            matches = face_recognition.compare_faces(self.known_face_encodings, face_encodings[i], tolerance = 0.5 )
            # print(matches)
            for j in range(len(matches)):
                if matches[j] == True:
                    # set name
                    self.name = self.known_face_names[j]
                    # add to list for return
                    known_face_location = np.array(face_locations[i]) * 2.0
                    print("Name : ", self.name ," Localtion : ", known_face_location)
                    middle_y = (known_face_location[1] + known_face_location[3]) / 2
                    middle_x = (known_face_location[0] + known_face_location[2]) / 2
                    start_point = (int(known_face_location[1]),int(known_face_location[0]))
                    # start_point = (1,1)
                    # print(start_point)
                    end_point = (int(known_face_location[3]),int(known_face_location[2]))
                    # end_point = (255,255)
                    color = (255,0,0)
                    thickness = 2
                    cv2.rectangle(cv_image,start_point,end_point,color,thickness)
                    # print(middle_x,middle_y)
                    center_middle_y = (middle_y - 960)
                    center_middle_x = (middle_x - 540)
                    r_y = center_middle_y * 0.033
                    r_x = center_middle_x * 0.038
                    self.avg_r_x.append(r_x)
                    self.avg_r_y.append(r_y)
                    if len(self.avg_r_x) >= self.n_avg:
                        self.avg_r_x.pop(0)
                        self.avg_r_y.pop(0)
                    self.val_rx += 0.2 * sum(self.avg_r_x)/(len(self.avg_r_x))
                    self.val_ry += 0.2 * sum(self.avg_r_y)/(len(self.avg_r_y))

                    # print(self.avg_r_x, self.avg_r_y)

                    print(-self.val_rx, -self.val_ry)

                    self.add_angle_pan(-self.val_ry)
                    self.add_angle_tilt(-self.val_rx)

                    # return [val_rx, val_ry]
                    # self.moving_avg_x.append(center_middle_x)
                    # self.moving_avg_y.append(center_middle_y)
                    # if (len(self.moving_avg_x) == 5):
                    #     mean_x = ((self.moving_avg_x[0])+(self.moving_avg_x[1])+(self.moving_avg_x[2])+(self.moving_avg_x[3])+(self.moving_avg_x[4]))/5
                    #     mean_y = ((self.moving_avg_y[0])+(self.moving_avg_y[1])+(self.moving_avg_y[2])+(self.moving_avg_y[3])+(self.moving_avg_y[4]))/5
                    #     r_y = -(mean_y * 0.033)
                    #     r_x  = -(mean_x * 0.038)
                    #     self.moving_avg_x.pop(0)
                    #     self.moving_avg_y.pop(0)
                    #     print(self.moving_avg_x,self.moving_avg_y)
                    #     print(r_x, r_y)

                        #self.middle[1]
                        # if abs(r_y) > 6:
                        #     self.pan_degree = r_y
                        #     self.add_angle_pan(self.pan_degree)
                        # if abs(r_x) > 7:
                        #     self.tilt_degree = r_x
                        #     self.add_angle_tilt(self.tilt_degree)
                        # return [r_x, r_y]
        # cv2.imshow("pic",cv_image)
        # cv2.waitKey(1)
        return[0,0]  
     
                                          
            
            # if self.process_this_frame % 5 == 0:

            #     self.process_this_frame = 1
                
            #     print("processing")
            #     small_face_locations = face_recognition.face_locations(rgb_small_frame, model="cnn")
            #     self.face_encodings = face_recognition.face_encodings(rgb_small_frame, small_face_locations)
            #     self.face_locations = small_face_locations * 2
            #     print(self.face_locations)

            #     # self.face_names = []
            #     # for face_encoding in self.face_encodings:
                    
            #     #     matches = face_recognition.compare_faces(self.known_face_encodings, face_encoding, tolerance = 0.5 )
            #     #     self.name = "Unknown" #-------- If don't know show UNKNOWN
      
            #     #     if True in matches:
            #     #         first_match_index = matches.index(True)
            #     #         self.name = self.known_face_names[first_match_index]
            #     #         self.known_face_location.append(self.face_locations[])
                
            # elif self.process_this_frame % 5 == 1:

            #     gc.collect()

            # self.process_this_frame += 1
            # if (len(self.face_locations) != 0):
            #     self.middle[0] = (self.face_locations[0][0] + self.face_locations[0][2])/2 
            #     self.middle[1] = (self.face_locations[0][1] + self.face_locations[0][3])/2
            #     print(self.face_locations)
            #     print(self.name)
            # return self.middle


if __name__ == "__main__":
    try:
        face_reg = Face_Recognition()
        while not rospy.is_shutdown():
            if face_reg.facetrack :
                face_reg.recognition()
                #time.sleep(1)
                pass
    except rospy.ROSInterruptException:
        pass
